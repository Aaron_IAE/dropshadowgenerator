<?php
$quarter = $argv[1] ?? '';
if ($quarter == '') {
	die("Call this program with a folder name!\nLike this: > php " . $argv[0] . ' foldername');
}

define("DS_OFFSET", 10);
define("DS_STEPS", 10);

if ( ! is_dir('Proof Images_' . $quarter . '/thumb/ds')) {
	fwrite(STDOUT, 'Creating folders... ' . "\n");
	mkdir('Proof Images_' . $quarter . '/thumb/ds', 0777, true);
}
fwrite(STDOUT, 'starting loop...' . "\n");
foreach (new DirectoryIterator(__DIR__ . '/Proof Images_' . $quarter . '/') as $file) {
	fwrite(STDOUT, 'Processing ' . $file . "\n");
    if ($file->isDot()) {
        continue;
    }
    if ($file->getExtension() == 'png' || $file->getExtension() == 'PNG') {
		$filedata['name'] = $file->getFilename();
		$filedata['path'] = $file->getPath();
		fwrite(STDOUT, 'Cropping ' . $filedata['name'] . "\n");
		cropImage($filedata);
		fwrite(STDOUT, 'Resizing ' . $filedata['name'] . "\n");
		resizeImage($filedata);
		fwrite(STDOUT, 'Adding dropshadow to ' . $filedata['name'] . "\n");
        addDropShadow($filedata);
        fwrite(STDOUT, 'Processed ' . $filedata['name'] . "\n");
    }
}

function cropImage($src_)
{
    $src = $src_['path'] . '/' . $src_['name'];

    $targetWidth = 660;

    list($o_width, $o_height) = getimagesize($src);

    $original_image = imagecreatefrompng($src);
    $image = imagecreatetruecolor($targetWidth, $o_height);

    $leftOffset = ($o_width - $targetWidth) / 2;

    imagecopy($image, $original_image, 0, 0, $leftOffset, 0, $targetWidth, $o_height);

    imagepng($image, $src);

    imagedestroy($original_image);
    return true;
}

function resizeImage($src_)
{
    $src = $src_['path'] . '/' . $src_['name'];

    list($o_width, $o_height) = getimagesize($src);

    $image = imagecreatefrompng($src);

    $small_image = imagescale($image, 200);

    $height = imagesy($small_image);

    if ($height > 500) {
        $rect = [
			'x' => 0,
			'y' => 0,
			'width' => 200,
			'height' => 500
        ];
        imagecrop($small_image, $rect);
    }
    imagepng($small_image, $src_['path'] . './thumb/' . $src_['name']);
}

function addDropShadow($src_)
{
    $src = $src_['path'] . './thumb/' . $src_['name'];

    list($o_width, $o_height) = getimagesize($src);

    $width  = $o_width + DS_OFFSET;
    $height = $o_height + DS_OFFSET;

    $image = imagecreatetruecolor($width, $height);
    imageAlphaBlending($image, true);
    imageSaveAlpha($image, true);

    $bg = imagecolorallocatealpha($image, 255, 255, 255, 127);
    imagefill($image, 0, 0, $bg);

    $background = 0;

    $step_offset = ($background / DS_STEPS);

    $current_color = $background;

    for ($i = 0; $i < DS_STEPS; $i++) {
        $colors[$i] = imagecolorallocatealpha($image, round($current_color), round($current_color), round($current_color), 115);

        $current_color += $step_offset;
    }

    for ($i = 1; $i < count($colors); $i++) {
        $offset = DS_OFFSET * ($i / DS_STEPS);
        imagefilledrectangle($image, DS_OFFSET - $offset, $offset, $o_width + $offset, $o_height + $offset, $colors[$i]);
    }

    $original_image = imagecreatefrompng($src);
    imageAlphaBlending($original_image, true);
    imageSaveAlpha($original_image, true);

    imagecopymerge($image, $original_image, DS_OFFSET, 0, 0, 0, $o_width, $o_height, 100);

    imagepng($image, $src_['path'] . './thumb/ds/' . $src_['name']);

    imagedestroy($image);
    imagedestroy($original_image);
}

function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct)
{
    // creating a cut resource
    $cut = imagecreatetruecolor($src_w, $src_h);

    // copying relevant section from background to the cut resource
    imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);

    // copying relevant section from watermark to the cut resource
    imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);

    // insert cut resource to destination image
    imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);
}
