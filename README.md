Trims all files in a directory to 650px wide (at their original height) and overwrites them.
Then scales those files to 200px wide, and crops them to be no longer than 500px, and saves them with the original file name in the thumb/ subdirectory.
Finally, takes each thumb image and addes a dropshadow (with transparency) to the bottom-left. These files are saveed with the original filename in the thumb/ds/ directory.

To call this script, use:  
php dropshadow.php QuarterName

example:  
> php dropshadow.php 2017Q4

Creates (if they don't already exist)  
./Proof Images 2017Q4  
./Proof Images 2017Q4/thumb  
./Proof Images 2017Q4/thumb/ds/  

and processes all PNG files in ./Proof Images 2017Q4
